// Request a node to be allocated to us
node( "Appimage1404" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Kdenlive Code
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'kdenlive/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/kdenlive']]
			]

		}

		// Now retrieve the artifacts
		stage('Retrieving Dependencies') {
			// First we grab the artifacted dependencies built last time round
			copyArtifacts filter: 'kdenlive-appimage-deps.tar', projectName: 'Kdenlive_Nightly_Appimage_Dependency_Build'

			// Now we unpack them
			sh """
				mkdir -p $HOME/appimage-workspace/
				cd $HOME/appimage-workspace/
				tar -xf $WORKSPACE/kdenlive-appimage-deps.tar
			"""
		}

		// Let's build Kdenlive now that we have everything we need
		stage('Building Kdenlive') {
			// The first parameter to the script is where the scripts should work - which is our workspace in this case
			// Otherwise we leave everything in the hands of that script
			sh """
				export PATH=$HOME/tools/bin/:$PATH

				kdenlive/packaging/appimage/build-kdenlive.sh $HOME/appimage-workspace/ $WORKSPACE/kdenlive/
			"""
		}

		// Now we can generate the actual Appimages!
		stage('Generating Kdenlive Appimage') {
			// The scripts handle everything here, so just run them
			sh """
				export PATH=$HOME/tools/bin/:$PATH

				kdenlive/packaging/appimage/build-image.sh $HOME/appimage-workspace/ $WORKSPACE/kdenlive/
				mv $HOME/appimage-workspace/*.appimage $WORKSPACE/
			"""
		}

		// Finally we capture the appimages for distribution to users
		stage('Capturing Appimages') {
			// We use Jenkins artifacts for this to save having to setup additional infrastructure
			archiveArtifacts artifacts: '*.appimage', onlyIfSuccessful: true
		}
	}
}
}
