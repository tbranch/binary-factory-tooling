import json
import yaml
import os
import re
import requests
import zipfile
import multiprocessing
import subprocess
import xml.etree.ElementTree as ET
import xdg.DesktopEntry
import tempfile

dest = "repo"
# if dest directory doesn't exist, run fdroid init
# https://f-droid.org/en/docs/Setup_an_F-Droid_App_Repo/

#aaptCall = "docker-kde-android.sh /opt/android-sdk/build-tools/21.1.2/aapt dump badging /pwd/%s"
aaptCall = "aapt dump badging %s"
#fdroid = "docker run --rm -u $(id -u):$(id -g) -v $(pwd):/repo registry.gitlab.com/fdroid/docker-executable-fdroidserver:latest"
fdroid = "fdroid"

def readAppName(apkpath):
    manifest = subprocess.check_output(aaptCall % (apkpath), shell=True).decode('utf-8')
    result = re.search(' name=\'([^\']*)\'', manifest)
    return result.group(1)

def readText(elem, found):
    lang = elem.get('{http://www.w3.org/XML/1998/namespace}lang')

    if not lang in found:
        found[lang] = ""

    if elem.tag == 'li':
        found[lang] += "· "
    if elem.text:
        found[lang] += elem.text
        for child in elem:
            readText(child, found)

    if elem.tag == 'li' or elem.tag == 'p':
        found[lang] += "\n"

conv = {
    None: "en-US",
    "ca": "ca-ES"
}
def createFastlaneFile(appname, filename, locales):
    for lang, text in locales.items():
        path = 'metadata/' + appname + '/' + conv.get(lang, lang)
        os.makedirs(path, exist_ok=True)
        with open(path + '/' + filename, 'w') as f:
            f.write(text)

def createYml(appname, data):
    info = {}
    path = 'metadata/' + appname + '.yml'
    with open(path, 'r') as contents:
        info = yaml.load(contents)

    info['Categories'] = data['categories'][None] + ['KDE']

    info['Summary'] = data['summary'][None]
    if 'url-homepage' in data:
        info['WebSite'] = data['url-homepage'][None]
    if 'url-bugtracker' in data:
        info['IssueTracker'] = data['url-bugtracker'][None]
    with open(path, 'w') as output:
        yaml.dump(info, output, default_flow_style=False)


def appdataContents(apkpath, name):
    with zipfile.ZipFile(apkpath, 'r') as contents:

        #https://docs.fastlane.tools/actions/supply/#images-and-screenshots

        data = {}
        with contents.open("assets/share/metainfo/%s.appdata.xml" % name) as appdataFile:
            root = ET.fromstring(appdataFile.read())
            for child in root:
                output = {}

                tag = child.tag
                if 'type' in child.attrib:
                    tag += '-' + child.attrib['type']

                if tag in data:
                    output = data[tag]

                if tag == 'categories':
                    cats = []
                    for x in child:
                        cats.append(x.text)
                    output = { None: cats }
                else:
                    readText(child, output)
                data[tag] = output

        if not 'categories' in data:
            with contents.open("assets/share/applications/%s.desktop" % name) as desktopFileContents:
                (fd, path) = tempfile.mkstemp(suffix=name + ".desktop")
                handle = open(fd, "wb")
                handle.write(desktopFileContents.read())
                handle.close()

                desktopFile = xdg.DesktopEntry.DesktopEntry(path)
                data['categories'] = { None: desktopFile.getCategories() }

        try:
            createFastlaneFile(name, "title.txt", data['name'])
            createFastlaneFile(name, "short_description.txt", data['summary'])
            createFastlaneFile(name, "full_description.txt", data['description'])
            createYml(name, data)
        except KeyError as e:
            print("error: key not found", e)


def fetch(job):
    name = job['name']

    zipfilename = "archive-%s.zip" % name
    with open(zipfilename, mode='wb') as f:
        url = "https://binary-factory.kde.org/view/Android/job/%s_android/lastSuccessfulBuild/artifact/*zip*/archive.zip" % name
        print("getting...", url)

        response = requests.get(url)
        data = response.content
        f.write(data)
        print("written", len(data), zipfilename)

    apks = []
    with zipfile.ZipFile(zipfilename, 'r') as contents:
        for entry in contents.namelist():
            if entry.endswith('-release-signed.apk'):
                filename = dest + '/' + entry[entry.rfind('/')+1:]
                with open(filename, mode='wb') as f:
                    f.write(contents.read(entry))
                print("created", filename)

                apks.append(filename)
    if len(apks) == 0:
        print("could not find apk for %s" % (name))
    return apks

def decorate(filenames):
    if len(filenames) == 0:
        return
    print("doing...", filenames)
    appname = readAppName(filenames[0])
    try:
        appdataContents(filenames[0], appname)
    except KeyError as e:
        print("could not inspect", appname, e, filenames[0])

with open("current-jobs.json") as f:
    jobs = json.load(f)

    pool = multiprocessing.Pool(8)
    filenames = pool.map(fetch, jobs)

    subprocess.check_call(fdroid + " update -c", shell=True)

    pool.map(decorate, filenames)

    subprocess.check_call(fdroid + " update", shell=True)
    subprocess.check_call(fdroid + " server update -v", shell=True)



